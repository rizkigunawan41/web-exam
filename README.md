# Latihan

Buat tampilan web seperti dibawah ini

## Persyaratan

1. Siapkan file html
2. Tambahkan element `<img />`, letakkan di dalam tag `body`
3. Atribut `src` atau source gambar menggunakan gambar stikom atau link [berikut](https://www.stikom-bali.ac.id/id/wp-content/uploads/2020/08/ITB-RESMI-MINI.png)
4. Berikan `height` dan `weight` sebesar `200` pada gambar tersebut
5. Tambahkan element heading 1 atau `h1` dan berikan teks "Pendaftaran Mahasiswa"
6. Tambahkan element `form`
7. Di dalam element `form`, tambahkan element `label`, berikan teks "Nama" di dalam element `label`.
8. Berikan element `<br />` di bawah element `label` untuk membuat element selanjutnya berada di bawah label.
9. Di bawah `<br />` tambahkan element `input` dengan tipe teks dan berikan `name` nya "nama" (sesuai dengan label).
10. Begitu seterusnya dengan `input` selanjutnya dengan tipe `number`, `date`, `email`.
11. Tambahkan element `button` berikan teks "DAFTAR"
